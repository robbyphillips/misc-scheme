;;;; grids.scm
;;;
;;; helper functions for grids!
;;;
;;;

(load "coords.scm")

(define null-grid ())

(define (make-grid xsize ysize zsize)
  (cons (list 'GRID xsize ysize zsize) 
        (make-grid-skeleton xsize ysize zsize)))

(define (grid-contents g) (cdr g))
(define (grid-header g) (car g))
(define (grid-xsize g) (cadar g))
(define (grid-ysize g) (caddar g))
(define (grid-zsize g) (car (cdddar g)))

(define (grid-item coords g)
  (cadr (assoc coords (grid-contents g))))


(define (make-grid-skeleton xsize ysize zsize)
  (define (helper axis)
    (cond ((null? axis) ())
          ((= 0 (car axis)) ())
          (else
            (cons (helper (cdr axis)) (helper (cons (- (car axis) 1) (cdr axis)))))))
  (helper `(,xsize ,ysize ,zsize)))


(define (grid-get g x y z)
  (list-ref (list-ref (list-ref (grid-contents g) x) y ) z))

(define (grid-set! g x y z val)
  (define (helper axis z)
    (cond ((null? axis) 'OUTTA-BOUNDS)
          ((= z 0)
           (set-car! axis val)
           axis)
          (else
            (cons (car axis (helper (cdr axis) (- z 1)))))))
  (helper (list-ref (list-ref (grid-contents g) x) y) z))

(define b
  '((grid 2 2 1) ((()) (())) ((()) (()))))
