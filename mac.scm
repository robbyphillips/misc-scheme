;;;; playing with macros!
;;; in mit-scheme
;;;

;; DEF-MULTI
;; calls define on multiple symbol/values
;; INPUTS: 2 lists of equal length, first of symbols second of values
;; OUTPUT: none
;;     EX: (def-multi (a b c) (1 2 3))
;;         a => 1 ; b => 2 ; c => 3
(define-syntax def-multi
  (syntax-rules ()
    ((_ (var ...) (expr ...))
      (begin
        (define var expr)
        ...))))

;; MULTI-DEF
;; calls define on multiple symbol value pairs
;; EX: (multi-def (a 1) (b 2))
;;     a => 1 ; b => 2
(define-syntax multi-def
  (syntax-rules ()
    ((_ (var expr) ...)
     (begin
       (define var expr)
       ...))))

;; list of symbols->string
(define (append-symbol . ls)
  (string->symbol
    (let loop ((ls (cdr ls))
               (str (symbol->string (car ls))))
      (if (null? ls)
        str
        (loop (cdr ls) (string-append str "-" (symbol->string (car ls))))))))

(define (incf-i) (set! i (+ 1 i)) i)

(define tests
  (lambda (x)
    (if (> x 0)
      (begin
        (display (random-choice 'a 'b 'c))
        (newline)
        (tests (- x 1)))
      'done)))
