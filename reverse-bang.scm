;;; reverse! for a question on stack overflow
;;;

(define L '(1 2 3))

(define-syntax reverse!
  (syntax-rules ()
    ((_ sym)
     (begin
       (set! sym (reverse sym))))))
