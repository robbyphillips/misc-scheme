;;;; turn based game??!?!

(load "queue")

(define *phases* (make-no-write-cont-queue 'INIT 'MOVE 'ACTION))
(define *terrain-list* '(dirt))
(define *MAP* (build-board 10 10 1))
;(define *unit-queue* (make-cont-queue (get-all-units *MAP*)))
(define *players* ())

(define (get-units player grid) ())
(define (get-all-units grid)())

(define-structure unit
  (move 1)
  (dmg 2)
  (hp 3))

(define (random-choice lst)
  (list-ref lst (random (length lst))))

(define (build-board x-size y-size z-size)
  (let loop ((board (make-equal-hash-table (* x-size y-size z-size)))
             (x x-size)
             (y y-size)
             (z z-size))
    (cond ((< x 0) board)
          ((< y 0) (loop board (- x 1) y-size z))
          ((< z 0) (loop board x (- y 1) z-size))
          (else
            (hash-table/set! board `(,x ,y ,z) (random-choice terrain-list))
            (loop board x y (- z 1))))))
