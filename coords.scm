;;;; Cartesian.scm
;;;
;;; Helpers, etc for dealing with cartesian coordinates.
;;;
;;;

(load "sorting.scm")

(define (make-point x y z) (list x y z))

(define (getx pt) (car pt))

(define (gety pt) (cadr pt))

(define (getz pt) (caddr pt))

;; Constants ;;;;;;;;;;;;;;;;;;;;;;;;
(define null-ptl ())                ;
(define null-pt ())                 ;
(define origin (make-point 0 0 0))  ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (point? pt)
  (and (= (length pt) 3) (all-ints? pt)))

(define (all-ints? l)
  (cond ((null? l) #t)
        ((integer? (car l))
         (all-ints? (cdr l)))
        (else #f)))

(define (make-ptl p ptl) (cons p ptl))

(define (ptl? l)
  (cond ((null? l) #t)
        ((point? (get-first-point l))
         (ptl? (get-rest-points l)))
        (else #f)))

(define (get-first-point ptl) (car ptl))

(define (get-rest-points ptl) (cdr ptl))

;; max-coord
;; returns the greatest x, y, or z coord in a list of points
;; INPUTS: Exactly 2 args getter, ptl
;; getter - (or getx gety getz) 
;;    ptl - list of points
;; OUTPUT: Numeric Value
(define (max-coord getter ptl)
  (define (helper l val)
    (cond ((null? l) val)
          (else
            (helper (get-rest-points l) (max (getter (get-first-point l)) val)))))
  (helper ptl 0))

;; distance
;; Calculates the distance between two points using the distance formula
;; INPUTS: Exactly 2 args p1 and p2, points
;; OUTPUT: numeric value
(define (distance p1 p2)
  (sqrt (+ (square (- (getx p1) (getx p2)))
           (square (- (gety p1) (gety p2)))
           (square (- (getz p1) (gety p2))))))

(define (sum-coord getter ptl)
  (cond ((null? ptl) 0)
        (else
          (+ (getter (get-first-point ptl)) (sum-coord getter (get-rest-points ptl))))))

;; max-distance
;; finds the maximum distance between a point and all points in a point list
;; INPUTS: Exactly 2 args p and pt-list
;;      p - point
;;    ptl - list of points
;; OUTPUT: Numeric value
(define (max-distance p ptl)
  (define (helper l d)
      (cond ((null? l) d)
	    (else (helper (get-rest-points l) (max d (distance p (get-first-point l)))))))
  (helper ptl 0))

;; min-distance
;; finds the minimum distance between a point and all points in a point list
;; INPUTS: Exactly 2 args p and ptl
;;      p - point
;;    ptl - list of points
;; OUTPUT: Numeric value
(define (min-distance p ptl)
  (define (helper l d)
    (cond ((null? l) d)
          (else
             (helper (get-rest-points l) (min d (distance p (get-first-point l)))))))
  (helper ptl (distance p (get-first-point ptl))))

;; farthest-point
;; returns the point in ptl that is farthest from p
;; INPUTS: Exactly 2 args p and ptl
;;      p - point
;;    ptl - list of points
;; OUTPUT: point
(define (farthest-point p ptl)
  (define (helper ptl d val)
    (cond ((null? ptl) val)
          ((> (distance p (get-first-point ptl)) d)
           (helper (get-rest-points ptl) (distance p (get-first-point ptl)) (get-first-point ptl)))
          (else
            (helper (get-rest-points ptl) d val))))
  (helper ptl 0 null-pt))

;; closest-point
;; returns the point in ptl that is closest to p
;; INPUTS: Exactly 2 args p and ptl
;;      p - point
;;    ptl - list of points
;; OUTPUT: point
(define (closest-point p ptl)
  (define (helper ptl d val)
    (cond ((null? ptl) val)
          ((< (distance p (get-first-point ptl)) d)
           (helper (get-rest-points ptl) (distance p (get-first-point ptl)) (get-first-point ptl)))
          (else
            (helper (get-rest-points ptl) d val))))
  (helper ptl (distance p (get-first-point ptl)) null-pt))

;; max-range
;; finds the greatest distance between any two points in a point list
;; INPUTS: Exactly one arg ptl, a list of points
;; OUTPUT: Numeric value
(define (max-range ptl)
  (define (helper l val)
    (cond ((null? l) val)
	  (else
	   (helper (get-rest-points l) (max val (max-distance (get-first-point l) ptl))))))
  (helper ptl 0))

;; make-sorted-ptl
;; Returns a sorted list of points containing p and ptl
;; INPUTS: Exactly 2 args p and ptl, where p is a point and ptl is an already sorted point list.
;; OUTPUT: point list
(define (make-sorted-ptl p ptl)
  (make-sorted-list closer-to-origin? p ptl))

;; sort-ptl
;; returns a sorted list of points of least to greatest distance from the origin
;; INPUTS: Exactly one arg ptl, a list of points
;; OUTPUT: point list
(define (sort-ptl ptl)
  (sort-list-by closer-to-origin? ptl))

;; closer-to-origin?
;; predicate that returns #t if first point is closer to origin than the second point, etc
;; INPUTS: an arbitrary number of points
;; OUTPUT: boolean
(define (closer-to-origin? . ptl)
  (apply < (map distance-from-origin ptl)))

;; distance-from-origin
;; returns the distance from a point to the origin
(define (distance-from-origin p)
  (distance p origin))

;; make-plane
;; 2 dimensional version of make-set
(define (make-plane xsize ysize)
  (make-set xsize ysize 1))

;; make-set
;; returns a point list with all integer points within the given size constraints starting at the origin
;; INPUTS: Exactly 3 args xsize, ysize, and zsize -- all integers
;; OUTPUT: point list
(define (make-set xsize ysize zsize)
  (define (helper xcount ycount zcount val)
    (cond ((> 0 zcount) val)
          ((> 0 ycount) (helper xcount (- ysize 1) (- zcount 1) val))
          ((> 0 xcount) (helper (- xsize 1) (- ycount 1) zcount val))
          (else
            (helper (- xcount 1) ycount zcount (make-ptl (make-point xcount ycount zcount) val)))))
  (helper (- xsize 1) (- ysize 1) (- zsize 1) null-ptl))

;; all-pts-within
;; returns a point list with all points closer than d to p
;; INPUTS: exactly 3 args d, p, and ptl
;;      d - numeric value
;;      p - point
;;    ptl - point list
;; OUTPUT: point list
(define (all-pts-within d p ptl)
  (define (helper ptl val)
    (cond ((null? ptl) val)
          ((< (distance p (get-first-point ptl)) d)
           (helper (get-rest-points ptl) (make-sorted-ptl (get-first-point ptl) val)))
          (else
            (helper (get-rest-points ptl) val))))
  (helper ptl null-ptl))

(define (next-coord p xlimit ylimit zlimit)
  (cond ((< (1+ (getx p)) xlimit) (make-point (1+ (getx p)) (gety p) (getz p)))
        ((< (1+ (gety p)) ylimit) (make-point (getx p) (1+ (gety p)) (getz p)))
        ((< (1+ (getz p)) zlimit) (make-point (getx p) (gety p) (1+ (getz p))))
        (else
          (error "Can't make new coord within limits"))))
