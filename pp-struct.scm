;;; printing the contents of a struct in chicken scheme

(use defstruct)

(defstruct tree height age leaf-color)

(define coconut (make-tree height: 30 age: 5 leaf-color: 'green))

(define (pp-tree t)
  (let loop ((attr (tree->alist t)))
    (cond ((null? attr) 'done)
          (else
            (display (caar attr))(display ": ")
            (display (cdar attr))(newline)
            (loop (cdr attr))))))
