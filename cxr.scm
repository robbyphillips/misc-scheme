;;;; Abstraction of car and cdr that takes in a string of an abitrary number of a's and d's
;;;  and returns a function that uses that information to find an element of a list 
;;;  EX: ((cxr "a") '(x y z)) => x
;;;  EX: ((cxr "ad") '(x y z)) => y
;;;
;;;

(define (cxr x)
  (define (helper xlist arg)
    (cond ((null? xlist) arg)
          ((eq? (car xlist) #\a) (car (helper (cdr xlist) arg)))
          ((eq? (car xlist) #\d) (cdr (helper (cdr xlist) arg)))
          (else (error "INVALID ARGS FOR CXR"))))
  (lambda (arg) (helper (string->list x) arg)))


(define (cxr2 x)
  (define (helper xlist arg)
    (cond ((null? xlist) arg)
          ((eq? (car xlist) #\a) (car (helper (cdr xlist) arg)))
          ((eq? (car xlist) #\d) (cdr (helper (cdr xlist) arg)))
          (else (error "INVALID ARGS FOR CXR"))))
  (eval `(define ,(string->symbol (string-append "c" (string-append x "r")))
           ,(lambda (arg) (helper (string->list x) arg))) user-initial-environment))
