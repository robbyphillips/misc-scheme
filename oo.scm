;;;; Let's play with object oriented programming!

(define def-employee
  (let ((id 0)
        (name-first "")
        (name-last "")
        (title "")
        (wage 0)
        (hours-year 0)
        (hours-pay-period 0)
        (overtime-total 0)
        (overtime-this-period 0)
        (shifts ()))
    (lambda (cmd attr)
      (cond ((equal? attr 'id) id)
            (else 'wut?)))))

(define counter
  (let ((count 0))
    (lambda ()
      (set! count (+ 1 count))
      count)))

(define next
  (let ((val 0))
    (lambda (x)
      (set! val (+ x val))
      val)))

(define (make-getters l)
  (cond ((null? l) '((else (error "UNKNOWN ATTR"))))
        (else
          (cons `((equal? (quote ,(caar l)) msg) ,(caar l))
                (make-getters (cdr l))))))

(define (make-setters l)
  (cond ((null? l) '((else (error "UNKNOWN ATTR"))))
        (else
          (cons `((equal? (quote ,(caar l)) msg) (set! ,(caar l) val) val)
                (make-setters (cdr l))))))
;;;;;;;;;;;;;;;;;;;;;;;;;
;;; IT WORKS.
(define def-prototype
  (lambda (proto-name . attributes)
    (eval
      `(define ,proto-name
         (let ,attributes
           (lambda (cmd msg #!optional val)
             (cond ((equal? cmd 'get)
                    ,(cons 'cond (make-getters attributes)))
                   ((and (equal? cmd 'set!) (equal? val #!default))
                    (error "SET! CALLED WITHOUT VALUE"))
                   ((equal? cmd 'set!)
                    ,(cons 'cond (make-setters attributes)))
                   (else (error "UNKNOWN CMD"))))))
      user-initial-environment)))

(define make-from-prototye
  (lambda (prototype . attributes) () ))

(define make-object
  (lambda (attributes #!optional methods)
    (eval
      `(let ,attributes
         (lambda (cmd #!optional msg val)
           (cond ((equal? cmd 'get)
                  ,(cons 'cond (make-getters attributes)))
                 ((equal? cmd 'set)
                  ,(cons 'cond (make-setters attributes)))
                 (else (error "UNKNOWN CMD")))))
      user-initial-environment)))

(define o
  (make-object
    '((type 'orange)
      (size 3)
      (color 'orange)
      (owner 'bobby))))

(define (a x)
  (define b
    (make-object
      `((type ,x)
        (q 5))))
  (b 'get 'q))

;;;;; Moving on ...
;;; consider a tagged data + msg passing implementation
;;; object = (obj type procedure)
;;; then automagically create generalized procedures:  
;;; (obj? object) =>
;;; (get-obj-type object) => (cadr object) => type
;;; (get-attr object 'attr) => ((caddr object) 'get 'attr)
;;; (set-attr! object 'attr val) => ((caddr object) 'set 'attr val)
;;;
;;; even better(?) :
;;; (get-type-attr object) => (if (type? (get-object-type object)) method)
;;; or some such...
