;;;; dcpu16 register help for stackoverflow question

;; a-list of registers and initial values
(define *reg*
  '((A . 0) (B . 0) (C . 0) (X . 0) (Y . 0) (Z . 0)
            (I . 0) (J . 0) (SP . #X10000) (PC . 0) (O . 0)
            (Pa . 0) (Pb . 0) (Paadr . 0) (Pbadr . 0) (CLK . 0)))

(define (reg-write register val)
  (set-cdr! (assoc register *reg*) val) ;write new value to register
  val) ; return newly written value

(define (reg-read register)
  (cdr (assoc register *reg*)))

(define (reg-inc register)
  (reg-write register (+ 1 (reg-read register))))

;; to do many operations
;; input:  a list of registers
;;    EX:  '(a b x)
(define (do-incs registers)
  (if (null? registers)
    'done
    (begin
      (reg-inc (car registers))
      (do-incs (cdr registers)))))
