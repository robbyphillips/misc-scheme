;;;; Conway's Game of Life
;;;;  (yet another version)
;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Author:  Rob Phillips
;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Creates an empty "grid" shaped list
;; Currently pointless as there is no way to set individual elements
(define (make-grid x-size y-size init)
  (map (lambda (y) (append (make-list x-size init) y)) (make-list y-size)))

;; Returns the value of a grid at coordinate x,y
;; INPUTS: exactly three arguments grid, x, y
;;          grid    -- a grid (two dimensional list)
;;          x       -- integer
;;          y       -- integer
;; OUTPUT: any type
(define (grid-ref grid x y)
  (list-ref (list-ref grid y) x))

(define (grid->list grid)
  (define x-size (length (car grid)))
  (define y-size (length grid))
  (define (helper x y result)
    (cond ((= y y-size) result)
          ((= x x-size)
           (helper 0 (+ y 1) result))
          (else
           (helper (+ x 1) y (append result (list (grid-ref grid x y)))))))
   (helper 0 0 '()))

;; list->grid
;; takes a list and an x-size and tries to make a grid
;; [note: it does not currently force an even grid if (/ (length l) x-size) is not 0 but it probably should]
;; INPUTS: Exactly two arguments l, x-size
;;          l       -- a list
;;          x-size  -- an integer
;; OUTPUT: list where each element is a list of at most x-size elements
;;     EX: (list->grid '(a b c d e f g h i) 3) => ((a b c) (d e f) (g h i))
(define (list->grid l x-size)
  (cond ((null? l) '())
        ((> x-size (length l)) l)
        (else
          (append (list (list-head l x-size)) (list->grid (list-tail l x-size) x-size))))) 

(define (map-grid proc g)
  (map (lambda (l) (map proc l)) g))

(define (live? grid x y)
  (let ((n (neighbors is1? grid x y)))
    (or (= n 2) (= n 3))))

(define (spawn? grid x y)
  (= (neighbors is1? grid x y) 3))

(define (is1? x) (= x 1))

;; neighbors
;; counts the number of neighbors surrounding a location on a grid according to a test
;; INPUTS: exactly 4 arguments
;;          test    -- a procedure that takes one arg and returns true or false 
;;          grid    -- a grid
;;          x       -- integer, x coord
;;          y       -- integer, y coord
;; OUTPUT: integer
;;     EX: (neighbors is1? '((0 0 0) (1 1 1) (0 0 0)) 1 1) => 2 
(define (neighbors test grid x y)
  (define x-size (length (car grid)))
  (define y-size (length grid))
  (define (helper a b x y)
    (cond ((> b 2) 0)
          ((> a 2) (helper 0 (+ 1 b) x y))
          ((and (= a b 1)) (helper (+ a 1) b x y))
          ((test (grid-ref grid (modulo (+ x a) x-size) (modulo (+ y b) y-size)))
           (+ 1 (helper (+ 1 a) b x y)))
          (else
           (helper (+ 1 a) b x y))))
  (helper 0 0 (modulo (- x 1) x-size) (modulo (- y 1) y-size))) 

(define (print-grid grid)
  (cond ((not (null? grid))
         (display (car grid)) (newline)
         (print-grid (cdr grid)))
        (else '())))

(define (pp-grid grid)
  (define (pp-row row)
    (cond ((null? row) (newline))
	  ((= 1 (car row)) (display "# ") (pp-row (cdr row)))
	  ((= 0 (car row)) (display "  ") (pp-row (cdr row)))))
  (cond ((null? grid) (display "----------") (newline))
        (else
          (pp-row (car grid))
          (pp-grid (cdr grid)))))

(define (init-conway x y density)
  (define (helper i cells)
    (cond ((= i (* x y)) cells)
          ((> density (random 100))
           (helper (+ i 1) (append '(1) cells)))
          (else
            (helper (+ i 1) (append '(0) cells)))))
  (list->grid (helper 0 '()) x))

(define (next-cycle grid)
  (define x-size (length (car grid)))
  (define y-size (length grid))
  (define (helper x y g)
    (cond ((>= y y-size) (list->grid g x-size))
          ((>= x x-size) (helper 0 (+ y 1) g))
          ((and (= 1 (grid-ref grid x y)) (live? grid x y))
           (helper (+ x 1) y (append g '(1))))
	  ((spawn? grid x y)
	   (helper (+ x 1) y (append g '(1))))
          (else
            (helper (+ x 1) y (append g '(0))))))
  (helper 0 0 '()))

(define (run-life grid cycles)
  (pp-grid grid)
  (if (> cycles 0)
    (run-life (next-cycle grid) (- cycles 1))
    'done!))

(define glider 
  '((0 1 0 0 0 0 0 0 0 0)
    (0 0 1 0 0 0 0 0 0 0)
    (1 1 1 0 0 0 0 0 0 0)
    (0 0 0 0 0 0 0 0 0 0)
    (0 0 0 0 0 0 0 0 0 0)
    (0 0 0 0 0 0 0 0 0 0)
    (0 0 0 0 0 0 0 0 0 0)
    (0 0 0 0 0 0 0 0 0 0)
    (0 0 0 0 0 0 0 0 0 0)
    (0 0 0 0 0 0 0 0 0 0)
    (0 0 0 0 0 0 0 0 0 0)))
