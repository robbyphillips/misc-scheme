;;; This is a test for mit-scheme environment variables and procedures
;;; DOCS: http://www.gnu.org/software/mit-scheme/documentation/mit-scheme-ref/Environments.html#Environments

(define a 100)
(define b 5)

(define foo
  (let ((c (+ a b))) ; c = 105
    (lambda (x y)
      (* x y c))))

(newline)
(display "Environment Procedures: ")(newline)(newline)

(display "Return current environment with (the-environment): ")(newline)
(display " => ") 
(display (the-environment))
(newline)(newline)

(display "Print the current environment with (pe): ")(newline)
(display " => ") 
(display (pe)) (newline)(newline)

(display "Return bound symbols with (environment-bindings [env])")(newline)
(display "(environment-bindings (the-environment))")(newline)
(display " => ")
(display (environment-bindings (the-environment)))
(newline)(newline)

(display "Get an environment with (ge [env])")(newline)
(display "Note that calling  just (ge [env]) will change the current REPL environment to [env]")
(newline) (display "(environment-bindings (ge foo))")(newline)
(display " => ") 
(display (environment-bindings (ge foo)))
(newline)(newline)

(display "Intern a symbol under a specific environment with") (newline)
(display " (environment-define [env] [symbol] [value]):")(newline)
(display " (environment-define (ge foo) 'z 40)")(newline)
(display " => ")
(display (environment-define (ge foo) 'z 40))
(newline) (display "(environment-bindings (ge foo))")(newline)
(display " => ") 
(display (environment-bindings (ge foo)))
(newline)(newline)

(define (flatten lst)
  (cond ((null? lst) ())
        ((pair? (car lst)) (append (flatten (car lst)) (flatten (cdr lst))))
        (else
          (cons (car lst) (flatten (cdr lst))))))

;;; GET-VAR-TYPES
;;; returns a list of pairs  containing each symbol in a form and the result
;;;  of calling `environment-reference-type` on that symbol once the form
;;;  has been evaluated under user-initial-environment.
;;; LIMITATIONS:  Does not see symbols bound in a let under a lambda as having
;;;  any value.  Will not work properly on quoted forms. Also, `flatten` is
;;;  really a bad way to go about parsing a scheme form since it destroys
;;;  almost all useful syntax.
;;; INPUTS: A quoted procedure form EX: '(lambda (x) (* x x))
;;; OUTPUT: List of pairs of the form ((x . unbound) (a . normal) (let . macro))
(define (get-var-types proc-form)
  (let ((env (ge (eval proc-form user-initial-environment))))
    (let loop ((pf (flatten proc-form))
               (out ()))
      (cond ((null? pf) out)
            ((symbol? (car pf))
             (loop (cdr pf) (cons (cons (car pf) (environment-reference-type env (car pf))) out)))
            (else
              (loop (cdr pf) out))))))

(define t '(lambda (x y) (let ((h 1) (f 3)) (+ (* x h) (* y f)))))
(define s '(let ((q 1) (r 2)) (lambda (x y) (= (q * x) (r * y)))))
(define r '(lambda (x) (let ((a 3)) (+ x 3))))

