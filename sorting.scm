;;;; Sorting Helpers
;;;
;;;
;;; Berto
;;;
;;;

;; make-sorted-list
;; maintains a sorted list while adding items
;; INPUTS: Exactly 3 args test, item, l
;;   test - predicate that takes 2 or more args
;;   item - thing to be added to list
;;      l - list
;; OUTPUT: list
(define (make-sorted-list test item l)
  (cond ((null? l) (cons item ()))
        ((test item (car l))
         (cons item l))
        (else
          (cons (car l) (make-sorted-list test item (cdr l))))))

;; sort-list-by
;; returns a list sorted by test
;; INPUTS: Exactly 2 args test and l
;;   test - predicate that takes 2 or more args
;;      l - list
;; OUTPUT: list
(define (sort-list-by test l)
  (cond ((null? l) ())
        (else
          (make-sorted-list test (car l) (sort-list-by test (cdr l))))))
